<?php
/*
Plugin Name: wpslider2
Description: wordpress slider plugin
Version: 0.0.1
*/

class WPSlider
{
	public static $url;
	public static $db;
	public static $id;

	public function __construct() 
	{
		register_activation_hook(__FILE__,[$this,"onactivation"]);

		global $wpdb;
		self::$db = $wpdb;
		self::$url = plugins_url("wpslider");

		//require_once("savepost.php");
		require_once("shortcode.php");
		require_once("pages.php");			
		require_once("customtypepost.php");			
		require_once("widget.php");
		require_once("uploadmedia.php");

	}

	public function onactivation() {
		$table_name = WPSlider::$db->prefix.'wpslider_images';
		if ( WPSlider::$db->get_var("SHOW TABLES LIKE '".$table_name."'") != $table_name ) {
			$sql = 	"CREATE TABLE `".WPSlider::$db->prefix."wpslider_images` ( ".
					"`id` int(11) NOT NULL, ".
					"`post_id` int(11) NOT NULL, ".
			  		"`image_id` int(11) NOT NULL, ".
			  		"`url` varchar(200) NOT NULL, ".
			  		"`text` varchar(200) NOT NULL, ".
			  		"`link` varchar(200) NOT NULL ".
					") ENGINE=InnoDB DEFAULT CHARSET=utf8; ";
			$res = WPSlider::$db->query($sql);				
			$sql =	"ALTER TABLE `".WPSlider::$db->prefix."wpslider_images` ".
			  		"ADD PRIMARY KEY (`id`); ";
			$res = WPSlider::$db->query($sql);						  	
			$sql =	"ALTER TABLE `".WPSlider::$db->prefix."wpslider_images` ".
			  		"MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0; ";
			$res = WPSlider::$db->query($sql);						  	
		}	
	}
}

new WPSLider();




