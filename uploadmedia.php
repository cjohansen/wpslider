<?php

class Upload_Media {

	public function __construct() {
		add_action( 'admin_footer', [$this,'media_selector_print_scripts'] );
	}

	function media_selector_print_scripts() {
		$my_saved_attachment_post_id = get_option( 'media_selector_attachment_id', 0 );
		require_once("assets/js/wpslider-upload-media-js.php");
	}
}

new Upload_Media();
