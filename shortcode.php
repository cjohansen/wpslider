<?php

class Shortcode 
{
	public function __construct() {
		add_shortcode('wpslider', [$this, 'sc_wpslider']);		
	}

	public function sc_wpslider($attr) {
		$post_title = $attr["id"];
		require("views/wpslider-entry.php");		
	}
}

new Shortcode();




?>