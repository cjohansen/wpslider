<p>
	<span>title:</span>
	<span>
		<input type="text" name="<?=$this->get_field_name("title")?>" value="<?=$title?>" >
	</span>
</p>


<p>
	<select name="<?=$this->get_field_name("wpslider")?>">
		<?php
			$query = new WP_Query( ["post_type" => "wpslider"] );
			$posts = $query->get_posts();

			print "<option>";
			foreach ( $posts as $v ) {
				if ( $v->post_title == $wpslider ) $selected = " selected ";
				else $selected = ""; 
				print "<option ".$selected.">".$v->post_title;
			}
		?>
	</select>
</p>
